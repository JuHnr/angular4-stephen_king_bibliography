import { Injectable } from '@angular/core';


const BASE_API_URL = "https://openlibrary.org";
const STEPHEN_KING_KEY = '/authors/OL2162284A'; //Key for Stephen King 
const WORKS = "/works.json?limit=50"; 

@Injectable({
  providedIn: 'root'
})

export class APIService {
  constructor() { }

  public async getBooks() {

    try {
      const data = await fetch(BASE_API_URL + STEPHEN_KING_KEY + WORKS);
      const response = await data.json();
      return response;
    } catch (error) {
      console.error(error);
      throw error;
    }

  }

  public async getDetails(bookKey: string) {
    if (bookKey) {
      try {
        const data = await fetch(BASE_API_URL + "/search.json?q=stephen+king+" + bookKey);
        const response = await data.json();
        return response;
      } catch (error) {
        console.error(error);
        throw error;
      }
    }
  }

  public async getDescriptionAndCover(bookKey: string) {
    if (bookKey) {
      try {
        const data = await fetch(BASE_API_URL + bookKey +".json");
        const response = await data.json();
        return response;
      } catch (error) {
        console.error(error);
        throw error;
      }
    }
  }
}
