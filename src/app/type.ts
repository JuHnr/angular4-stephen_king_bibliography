export type Book = {
    key: string;
    title: string;
    covers: number[];
    description: { type: string, value: string };
    excerpts: { excerpt: string; comment: string; author: string }[];
}

export type BookDetails = {
    already_read_count: number;
    author_key: string[];
    author_name: string[];
    currently_reading_count: number;
    ebook_access: string;
    ebook_count_i: number;
    edition_count: number;
    first_publish_year: number;
    isbn: string[];
    key: string;
    language: string[];
    last_modified_i: number;
    number_of_pages_median: number;
    person: string[];
    place: string[];
    publish_date: string[];
    publish_place: string[];
    publish_year: number[];
    publisher: string[];
    ratings_average: number;
    ratings_count: number;
    ratings_sortable: number;
    readinglog_count: number;
    subject: string[];
    time: string[];
    title: string;
    want_to_read_count: number;

};

