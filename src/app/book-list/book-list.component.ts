import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';
import { RouterLink } from '@angular/router';
import { Book, BookDetails } from '../type';
import { FormsModule } from '@angular/forms';
import { CoverUrlPipe } from '../pipes/cover-url.pipe';



@Component({
  selector: 'app-book-list',
  standalone: true,
  imports: [RouterLink, FormsModule, CoverUrlPipe],
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent implements OnInit {
  public books: Book[] = [];
  public filteredBooks: Book[] = [];
  public headerText: string = "";
  public headerSubtext: string = "";

  constructor(public apiService: APIService) { }

  ngOnInit(): void {

    this.headerText = "All books";
    this.headerSubtext = "";

    this.apiService.getBooks()
      .then((response) => {
        this.books = response.entries; //liste complète
        this.books = this.books.sort((a, b) => a.title.localeCompare(b.title));
        this.filteredBooks = this.books; //utilise la liste filtrée et triée à l'affichage
      })

  }

  /**
   * Permet de filtrer sur le titre selon des termes de recherches.
   */
  public filterResults(text: string): void {
    this.headerText = text;
    this.headerSubtext = "Search results :";
    //si aucun terme de recherche entré, alors renvoie la liste complète
    if (!text) {
      this.filteredBooks = this.books;
      return;
    }
    //sinon filtre la liste par correspondance des termes
    this.filteredBooks = this.books.filter(
      book => book?.title.toLowerCase().includes(text.trim().toLowerCase())
    );

    if (this.filteredBooks.length === 0) {
      this.headerSubtext = "No result found";
      this.filteredBooks = this.books;
    }
  }

  /**
    * Permet de trier les titres des livres par popularité (les plus notés).
    */
  public async SortBooksByPopularity(): Promise<void> {
    this.headerText = "All books";
    this.headerSubtext = "by Number of pages (highest)";
    // Crée un tableau de promesses pour récupérer les détails de chaque livre
    const detailsPromises = this.books.map(book => this.apiService.getDetails(book.key));
    // Attend que toutes les requêtes soient terminées
    const detailsResponses = await Promise.all(detailsPromises);

    // Filtrer et trier les livres en fonction de nombre de lecteur
    this.filteredBooks = this.books.filter((book, index) => {
      const bookDetails = detailsResponses[index].docs[0];
      // Vérifie que bookDetails est défini et qu'il a la propriété already_read_count
      return bookDetails && bookDetails.already_read_count;

      //TODO afficher avec tri
    });
  }


  /**
    * Permet de filtrer par genre.
    */
  public async FilterBooksByGenre(genre: string): Promise<void> {
    this.headerText = genre;
    this.headerSubtext = "Results : ";
    // Crée un tableau de promesses pour récupérer les détails de chaque livre
    const detailsPromises = this.books.map(book => this.apiService.getDetails(book.key));
    // Attend que toutes les requêtes soient terminées
    const detailsResponses = await Promise.all(detailsPromises);

    // Filtre les livres en fonction de leur genre
    this.filteredBooks = this.books.filter((book, index) => {
      //récupère les données du livre
      const bookDetails = detailsResponses[index].docs[0];

      // Si les données contiennent le tableau subject
      if (bookDetails.subject) {
        // Itère dans le tableau et vérifie la présence du genre avec la valeur
        for (const subjectItem of bookDetails.subject) {
          if (subjectItem.toLowerCase().includes(genre.toLowerCase())) {
            return true;
          }
        }
      }
      return false;
    });
  }

  /**
  * Permet de filtrer par theme.
  */
  public async FilterBooksByTheme(theme: string): Promise<void> {
    this.headerText = theme;
    this.headerSubtext = "Results : ";
    // Crée un tableau de promesses pour récupérer les détails de chaque livre
    const detailsPromises = this.books.map(book => this.apiService.getDetails(book.key));
    // Attend que toutes les requêtes soient terminées
    const detailsResponses = await Promise.all(detailsPromises);

    // Filtre les livres en fonction de leur thème
    this.filteredBooks = this.books.filter((book, index) => {
      //récupère les données du livre
      const bookDetails = detailsResponses[index].docs[0];

      // Si les données contiennent le tableau subject
      if (bookDetails.time) {
        // Itère dans le tableau et vérifie la présence du thème avec la valeur
        for (const element of bookDetails.time) {
          if (element.toLowerCase().includes(theme.toLowerCase())) {
            return true;
          }
        }
      }
      return false;
    });
  }

  /**
    * Permet de filtrer les livres en fonction de leur note moyenne.
    */
  public async filterBooksByRating(minRating: number): Promise<void> {
    this.headerText = "Top rated books";
    this.headerSubtext = "Rating average > 4";
    // Crée un tableau de promesses pour récupérer les détails de chaque livre
    const detailsPromises = this.books.map(book => this.apiService.getDetails(book.key));
    // Attend que toutes les requêtes soient terminées
    const detailsResponses = await Promise.all(detailsPromises);

    // Filtre les livres en fonction de leur note moyenne
    this.filteredBooks = this.books.filter((book, index) => {
      const bookDetails = detailsResponses[index].docs[0];
      //vérifie que bookDetails est défini et qu'il a la propriété ratings_average
      return bookDetails && bookDetails.ratings_average >= minRating;
    });
  }

  /**
    * Permet de réinitialiser la liste des livres à celle de base.
    */
  public resetBookList(): void {
    this.filteredBooks = this.books;
    this.headerText = "All books";
    this.headerSubtext = "";
  }


}
