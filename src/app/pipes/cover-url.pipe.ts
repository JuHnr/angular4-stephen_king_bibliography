import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coverUrl',
  standalone: true
})
export class CoverUrlPipe implements PipeTransform {

  transform(coversArray: number[]): string {
    if (coversArray && coversArray.length > 0 && coversArray[0] != -1) {
      const coverId = coversArray[0];
      // https://covers.openlibrary.org/b/id/416165161-M.jpg
      return "https://covers.openlibrary.org/b/id/" + coverId + "-M.jpg";
    } else {
      return "https://dataintelligence.at/wp-content/uploads/2023/09/Sourdough_Bread1.jpg";
    }
  }


}
