import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rating',
  standalone: true
})
export class RatingPipe implements PipeTransform {

  transform(rating: number | undefined): number | string {
    if (rating) {
      return parseFloat(rating.toFixed(1));
    }
    return "N/A"
  }

}
