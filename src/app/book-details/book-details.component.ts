import { Component, Input, OnInit, inject, input } from '@angular/core';
import { APIService } from '../services/api.service';
import { RouterLink } from '@angular/router';
import { Book, BookDetails } from '../type';
import { RatingPipe } from '../pipes/rating.pipe';


@Component({
  selector: 'app-book-details',
  standalone: true,
  imports: [RouterLink, RatingPipe],
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.css'
})
export class BookDetailsComponent implements OnInit {
  @Input() id: string = ""; //permet de récupérer l'id présent dans l'url
  public bookDetails?: BookDetails;
  public bookDescriptionAndCover?: Book; 
  public bookCover?: number; 
  public bookExcerpt?: string;

  constructor(public apiService: APIService) { }

  ngOnInit(): void {
    //si un id est bien présent en paramètre de l'url alors 
    if (this.id) {
      //récupère les données du livre correspondant
      this.apiService.getDetails(this.id)
        .then((response) => {
          this.bookDetails = response.docs[0];
          console.log(this.bookDetails); //TODO à supprimer
        })
      
      //récupère d'autres données du livre correspondant
      this.apiService.getDescriptionAndCover(this.id)
        .then((response) => {
          this.bookDescriptionAndCover = response;
          console.log(this.bookDescriptionAndCover);
          this.bookCover = this.bookDescriptionAndCover?.covers[0];
          this.bookExcerpt = this.bookDescriptionAndCover?.excerpts[0].excerpt;
        })

    }
  }

}
